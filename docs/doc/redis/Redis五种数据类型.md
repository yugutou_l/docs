# Redis五种数据类型

## string字符串类型

Redis的String能表达字符串、整数    

100.01是个六位的字符串

常见操作命令如下表：

| 命令名称 | 命令格式                             | 命令描述                |
| -------- | ------------------------------------ | ----------------------- |
| set      | set key value                        | 赋值                    |
| get      | get key                              | 取值                    |
| getset   | getset key value                     | 取值并赋值              |
| setnx    | setnx key value     set key value NX | 当value不存在时采用赋值 |
| append   | append key value                     | 向尾部追加值            |
| strlen   | strlen key                           | 获取字符串长度          |
| incr     | incr key                             | 递增数字                |
| incrby   | incrby key increment                 | 增加指定的整数          |
| decr     | decr key                             | 递减数字                |
| decrby   | decrby key decrement                 | 减少指定的整数          |

##### 应用场景：

##### 1 、普通的赋值

##### 2 、incr：递增数字

##### 3 、setnx：value不存在时采用赋值，可用于实现分布式锁

## list列表类型

list列表类型可以存储有序、可重复的元素

获取头部或尾部附近的记录是极快的

list的元素个数最多为2^32-1个（ 40 亿）

常见操作命令如下表：

| 命令名称  | 命令格式                                   | 命令描述                                                     |
| --------- | ------------------------------------------ | ------------------------------------------------------------ |
| lpush     | lpush key v v2 v3 ...                      | 从左侧插入列表                                               |
| lpop      | lpop key                                   | 从列表左侧取出                                               |
| rpush     | rpush key v v2 v3 ...                      | 从右侧插入列表                                               |
| rpop      | rpop key                                   | 从列表右侧取出                                               |
| blpop     | blpop key timeout                          | 从列表左侧取出，列表为空时阻塞多少秒                         |
| brpop     | blpop key timeout                          | 从列表右侧取出，列表为空时阻塞多少秒                         |
| llen      | llen key                                   | 获得列表中元素个数                                           |
| lindex    | lindex key index                           | 获得列表中下标为index的元素 index从 0 开始                   |
| lrange    | lrange key start end                       | 返回列表中start和end区间的元素                               |
| lrem      | lrem key count value                       | 删除列表中与value相等的元素<br />当count>0时， lrem会从列表左边开始删除;<br />当count<0时，lrem会从列表后边开始删除;<br />当count=0时， lrem删除所有值为value的元素 |
| lset      | lset key index value                       | 将列表index位置的元素设置成value的值                         |
| ltrim     | ltrim key start end                        | 对列表进行修剪，只保留start到end区间                         |
| rpoplpush | rpoplpush key1 key2                        | 从key1列表右侧弹出并插入到key2列表左侧                       |
| linsert   | linsert key BEFORE/AFTER <br />pivot value | 将value插入到列表，且位于值pivot之前或之后                   |

##### 应用场景：

##### 1 、列表有序可以作为栈和队列使用

##### 2 、可用于各种列表，比如用户列表、商品列表、评论列表等。

### set集合类型

Set：无序、唯一元素

集合中最大的成员数为 2^32 - 1

常见操作命令如下表：

| 命令名称    | 命令格式                | 命令描述                               |
| ----------- | ----------------------- | -------------------------------------- |
| sadd        | sadd key mem1 mem2 .... | 为集合添加新成员                       |
| srem        | srem key mem1 mem2 .... | 删除集合中指定成员                     |
| smembers    | smembers key            | 获得集合中所有元素                     |
| spop        | spop key                | 返回集合中一个随机元素，并将该元素删除 |
| srandmember | srandmember key         | 返回集合中一个随机元素，不会删除该元素 |
| scard       | scard key               | 获得集合中元素的数量                   |
| sismember   | sismember key member    | 判断元素是否在集合内                   |
| sinter      | sinter key1 key2 key3   | 求多集合的交集                         |
| sdiff       | sdiff key1 key2 key3    | 求多集合的差集                         |
| sunion      | sunion key1 key2 key3   | 求多集合的并集                         |

##### 应用场景：

##### 适用于不能重复的且不需要顺序的数据结构

##### 比如：关注的用户，朋友圈点赞,还可以通过spop进行随机抽奖

## sortedset有序集合类型

SortedSet(ZSet) 有序集合： 元素本身是无序不重复的

每个元素关联一个分数(score)

可按分数排序，分数可重复

常见操作命令如下表：

| 命令名称      | 命令格式                                   | 命令描述                                               |
| ------------- | ------------------------------------------ | ------------------------------------------------------ |
| zadd          | zadd key score1 member1 score2 member2 ... | 为有序集合添加新成员                                   |
| zrem          | zrem key mem1 mem2 ....                    | 删除有序集合中指定成员                                 |
| zcard         | zcard key                                  | 获得有序集合中的元素数量                               |
| zcount        | zcount key min max                         | 返回集合中score值在[min,max]区间的元素数量             |
| zincrby       | zincrby key increment member               | 在集合的member分值上加increment                        |
| zscore        | zscore key member                          | 获得集合中member的分值                                 |
| zrank         | zrank key member                           | 获得集合中member的排名（按分值从小到大）               |
| zrevrank      | zrevrank key member                        | 获得集合中member的排名（按分值从大到小）               |
| zrange        | zrange key start end                       | 获得集合中指定区间成员，按分数递增排序                 |
| zrevrange     | zrevrange key start end                    | 获得集合中指定区间成员，按分数递减排序                 |
| zrangebyscore | zrangebyscore key min max                  | 获得集合中score值在[min,max]区间的成员，按分数递增排序 |

##### 应用场景：

##### 由于可以按照分值排序，所以适用于各种排行榜。比如：点击排行榜、销量排行榜、关注排行榜等。

## hash类型（散列表）

Redis hash 是一个 string 类型的 field 和 value 的映射表，它提供了字段和字段值的映射。

每个 hash 可以存储 2^32 - 1 键值对（ 40 多亿）。

常见操作命令如下表：

| 命令名称 | 命令格式                              | 命令描述                    |
| -------- | ------------------------------------- | --------------------------- |
| hset     | hset key field value                  | 赋值，不区别新增或修改      |
| hmset    | hmset key field1 value1 field2 value2 | 批量赋值                    |
| hsetnx   | hsetnx key field value                | 赋值，如果filed存在则不操作 |
| hexists  | hexists key filed                     | 查看某个field是否存在       |
| hget     | hget key field                        | 获取一个字段值              |
| hmget    | hmget key field1 field2 ...           | 获取多个字段值              |
| hgetall  | hgetall key                           | 获取全部字段值              |
| hdel     | hdel key field1 field2...             | 删除指定字段                |
| hincrby  | hincrby key field increment           | 指定字段自增increment       |
| hlen     | hlen key                              | 获得字段数量                |

##### 应用场景：

##### 对象的存储 ，表数据的映射

