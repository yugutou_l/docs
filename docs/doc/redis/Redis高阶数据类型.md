# Redis高阶数据类型

## bitmap位图类型

bitmap是进行位操作的

通过一个bit位来表示某个元素对应的值或者状态,其中的key就是对应元素本身。

bitmap本身会极大的节省储存空间。

常见操作命令如下表：

| 命令名称 | 命令格式                                    | 描述                                     |
| -------- | :------------------------------------------ | ---------------------------------------- |
| setbit   | setbit key offset value                     | 设置key在offset处的bit值(只能是 0 或者1) |
| getbit   | getbit key offset                           | 获得key在offset处的bit值                 |
| bitcount | bitcount key                                | 获得key的bit位为 1 的个数                |
| bitpos   | bitpos key bit                              | 返回第一个被设置为bit值的索引值          |
| bitop    | bitop and[or/xor/not] destkey key1 key2 ... | 对多个key 进行逻辑运算后存入destkey      |

![20211201-190210-0393.png](./image/20211201-190210-0393.png)

![20211201-190011-0102.png](./image/20211201-190011-0102.png)

##### 应用场景：

##### 1 、用户每月签到，用户id为key ， 日期作为偏移量 1 表示签到

##### 2 、统计活跃用户, 日期为key，用户id为偏移量 1 表示活跃

##### 3 、查询用户在线状态， 日期为key，用户id为偏移量 1 表示在线

## geo地理位置类型

geo是Redis用来处理位置信息的

在Redis3.2中正式使用。主要是利用了geohash算法（Z阶曲线、Base32编码）

Z阶曲线：

在x轴和y轴上将十进制数转化为二进制数，采用x轴和y轴对应的二进制数依次交叉后得到一个六位数编

码。把数字从小到大依次连起来的曲线称为Z阶曲线，Z阶曲线是把多维转换成一维的一种方法。

Base32编码：

Base32这种数据编码机制，主要用来把二进制数据编码成可见的字符串，其编码规则是：任意给定一

个二进制数据，以 5 个位(bit)为一组进行切分(base64以 6 个位(bit)为一组)，对切分而成的每个组进行编

码得到 1 个可见字符。Base32编码表字符集中的字符总数为 32 个（0-9、b-z去掉a、i、l、o），这也是

Base32名字的由来。

geohash算法：

Gustavo在 2008 年 2 月上线了geohash.org网站。Geohash是一种地理位置信息编码方法。 经过

geohash映射后，地球上任意位置的经纬度坐标可以表示成一个较短的字符串。可以方便的存储在数据

库中，附在邮件上，以及方便的使用在其他服务中。以北京的坐标举例，[39.928167,116.389550]可以

转换成wx4g0s8q3jf9。

Redis中经纬度使用 52 位的整数进行编码，放进zset中，zset的value元素是key，score是GeoHash的

52 位整数值。在使用Redis进行Geo查询时，其内部对应的操作其实只是zset(skiplist)的操作。通过zset

的score进行排序就可以得到坐标附近的其它元素，通过将score还原成坐标值就可以得到元素的原始坐标。

| 命令名称          | 命令格式                                                     | 描述                     |
| ----------------- | ------------------------------------------------------------ | ------------------------ |
| geoadd            | geoadd key 经度1 纬度2 place1 经度2 纬度2 place2 ...         | 添加地理坐标             |
| geohash           | geohash key place1 place2 ...                                | 返回标准的geohash串      |
| geopos            | geopos key place1 place2 ...                                 | 返回成员经纬度           |
| geodist           | geodist key place1 place2 单位 (m,km)                        | 计算成员间距离           |
| georadius         | georadius key place 距离 单位 withcoord withdist count 数 asc[desc]<br />(withcoord：获得经纬度   withdist：获得距离    withhash：获得geohash码) | 根据经纬度查找附近的位置 |
| georadiusbymember | georadiusbymember key place 距离 单位 count 数 asc[desc]     | 根据位置查找附近的位置   |

![20211201-200607-0828.png](./image/20211201-200607-0828.png)

##### 应用场景：

##### 1 、记录地理位置

##### 2 、计算距离

##### 3 、查找"附近的人"，"附近的酒店"

## hyperLogLog

Redis在2.8.9中添加了hyperLogLog结构。

用来做基数统计，在输入元素数量或体积非常大时，计算基数所需的空间是固定且很小的

只会进行不重复的基数统计，而不会存储元素本身，所以hyperLogLog不能像集合一样返回元素

非精确统计，误差为0.81%左右

| 命令名称 | 命令格式                     | 描述                        |
| -------- | ---------------------------- | --------------------------- |
| pfadd    | pfadd key value1 value2 ...  | 将元素添加的key中           |
| pfcount  | pfcount key                  | 统计key的元素数量（不精确） |
| pfmerge  | pfmerge newkey key1 key2 ... | 合并key至新key              |

##### 应用场景：

##### 1 、统计网站的UV，统计文章的UV

##### 2 、统计用户搜索网站关键词的数量

## stream数据流类型

stream是Redis5.0后新增的数据结构，用于可持久化的消息队列。

几乎满足了消息队列具备的全部内容，包括：

- 消息ID的序列化生成

- 消息遍历

- 消息的阻塞和非阻塞读取

- 消息的分组消费

- 未完成消息的处理

- 消息队列监控

每个Stream都有唯一的名称，它就是Redis的key，首次使用 xadd 指令追加消息时自动创建。

常见操作命令如下表：

| 命令名称   | 命令格式                                                     | 描述                                                         |
| ---------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| xadd       | xadd key id <*> field1 value1....                            | 将指定消息数据追加到指定队列(key)中，*表示最新生成的id（当前时间+序列号） |
| xread      | xread [COUNT count] [BLOCK milliseconds] STREAMS key [key ...] ID [ID ...] | 从消息队列中读取，COUNT：读取条数， BLOCK：阻塞读（默认不阻塞）key：队列 名称 id：消息id |
| xrange     | xrange key start end [COUNT]                                 | 读取队列中给定ID范围的消息 COUNT：返 回消息条数（消息id从小到大） |
| xrevrange  | xrevrange key start end [COUNT]                              | 读取队列中给定ID范围的消息 COUNT：返 回消息条数（消息id从大到小） |
| xdel       | xdel key id                                                  | 删除队列的消息                                               |
| xgroup     | xgroup create key groupname id                               | 创建一个新的消费组                                           |
| xgroup     | xgroup destory key groupname                                 | 删除指定消费组                                               |
| xgroup     | xgroup delconsumer key groupname cname                       | 删除指定消费组中的某个消费者                                 |
| xgroup     | xgroup setid key id                                          | 修改指定消息的最大id                                         |
| xreadgroup | xreadgroup group groupname consumer COUNT streams key        | 从队列中的消费组中创建消费者并消费数据 （consumer不存在则创建） |

##### 应用场景：消息队列
