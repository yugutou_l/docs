# Redis扩展功能

## 事务

是指作为单个逻辑工作单元执行的一系列操作

- Redis的事务是通过multi、exec、discard和watch这四个命令来完成的。 
- Redis的单个命令都是原子性的，所以这里需要确保事务性的对象是命令集合。 
- Redis将命令集合序列化并确保处于同一事务的命令集合连续且不被打断的执行 
- Redis不支持回滚操作

### 事务命令

multi：用于标记事务块的开始,Redis会将后续的命令逐个放入队列中，然后使用exec原子化地执行这个命令队列

exec：执行命令队列

discard：清除命令队列

watch：监视key

unwatch：清除监视key

```bash
127.0.0.1:6379> multi
OK
127.0.0.1:6379> set s1 222
QUEUED
127.0.0.1:6379> hset set1 name zhangfei
QUEUED
127.0.0.1:6379> exec
1) OK
2) (integer) 1
127.0.0.1:6379> multi
OK
127.0.0.1:6379> set s2 333
QUEUED
127.0.0.1:6379> hset set2 age 23
QUEUED
127.0.0.1:6379> discard
OK
127.0.0.1:6379> exec
(error) ERR EXEC without MULTI
127.0.0.1:6379> watch s1
OK
127.0.0.1:6379> multi
OK
127.0.0.1:6379> set s1 555
QUEUED
127.0.0.1:6379> exec # 此时在没有exec之前，通过另一个命令窗口对监控的s1字段进行修改
(nil)
127.0.0.1:6379> get s1
222
```

### 事务的执行

1. 事务开始 在RedisClient中，有属性flags，用来表示是否在事务中 flags=REDIS_MULTI 
2. 命令入队 RedisClient将命令存放在事务队列中 （EXEC,DISCARD,WATCH,MULTI除外） 
3. 事务队列 multiCmd *commands 用于存放命令 
4. 执行事务 RedisClient向服务器端发送exec命令，RedisServer会遍历事务队列,执行队列中的命令,最后将执 行的结果一次性返回给客户端。

### Watch的执行

redisDb有一个watched_keys字典,key是某个被监视的数据的key,值是一个链表.记录了所有监视这个数 据的客户端。

当修改数据后，监视这个数据的客户端的flags置为REDIS_DIRTY_CAS

RedisClient向服务器端发送exec命令，服务器判断RedisClient的flags，如果为REDIS_DIRTY_CAS，则 清空事务队列。

### Redis的弱事务性

- Redis语法错误   整个事务的命令在队列里都清除  flags=multi_dirty 
- Redis运行错误   在队列里正确的命令可以执行 （弱事务性）

```bash
127.0.0.1:6379> multi
OK
127.0.0.1:6379> set m1 111
QUEUED
127.0.0.1:6379> sets m2 222
(error) ERR unknown command 'sets'
127.0.0.1:6379> exec
(error) EXECABORT Transaction discarded because of previous errors.
127.0.0.1:6379> get m1
(nil)

127.0.0.1:6379> multi
OK
127.0.0.1:6379> set m1 111
QUEUED
127.0.0.1:6379> lpush m1 1 2 3   #不是语法错误
QUEUED
127.0.0.1:6379> exec
1) OK
2) (error) WRONGTYPE Operation against a key holding the wrong kind of value
127.0.0.1:6379> get m1
"111"
```

弱事务性 ： 

1、在队列里正确的命令可以执行 （非原子操作） 

2、不支持回滚

## Lua脚本

lua是一种轻量小巧的脚本语言，用标准C语言编写并以源代码形式开放， 其设计目的是为了嵌入应用 程序中，从而为应用程序提供灵活的扩展和定制功能。

利用Redis整合Lua，主要是为了性能以及事务的原子性。因为redis帮我们提供的事务功能太差。

### Lua环境协作组件

从Redis2.6.0版本开始，通过内置的lua编译/解释器，可以使用EVAL命令对lua脚本进行求值。

脚本的命令是原子的，RedisServer在执行脚本命令中，不允许插入新的命令

### EVAL命令

EVAL script numkeys key [key ...] arg [arg ...]

```bash
eval "return {KEYS[1],KEYS[2],ARGV[1],ARGV[2]}" 2 key1 key2 first second
```

命令说明：

- script参数：是一段Lua脚本程序，它会被运行在Redis服务器上下文中，这段脚本不必(也不应该) 定义为一个Lua函数。 
- numkeys参数：用于指定键名参数的个数。 
- key [key ...]参数： 从EVAL的第三个参数开始算起，使用了numkeys个键（key），表示在脚本中 所用到的那些Redis键(key)，这些键名参数可以在Lua中通过全局变量KEYS数组，用1为基址的形 式访问( KEYS[1] ， KEYS[2] ，以此类推)。 
- arg [arg ...]参数：可以在Lua中通过全局变量ARGV数组访问，访问的形式和KEYS变量类似( ARGV[1] 、 ARGV[2] ，诸如此类)。

#### lua脚本中调用Redis命令

redis.call()：如果出错，则返回错误信息，不继续执行

redis.pcall()：如果出错，则记录错误信息，继续执行

### EVALSHA

Redis 有一个内部的缓存机制，因此它不会每次都重新编译脚本，不过在很多场合，付出无谓的带宽来传送脚本主体并不是最佳选择。

```bash
192.168.24.131:6380> script load "return redis.call('set',KEYS[1],ARGV[1])"
"c686f316aaf1eb01d5a4de1b0b63cd233010e63d"
192.168.24.131:6380> evalsha c686f316aaf1eb01d5a4de1b0b63cd233010e63d 1 n2 zhangfei
OK
192.168.24.131:6380> get n2
zhangfei
```

#### SCRIPT命令

- SCRIPT FLUSH ：清除所有脚本缓存 
- SCRIPT EXISTS ：根据给定的脚本校验和，检查指定的脚本是否存在于脚本缓存 
- SCRIPT LOAD ：将一个脚本装入脚本缓存，返回SHA1摘要，但并不立即运行它
- SCRIPT KILL ：杀死当前正在运行的脚本

### 管道（pipeline）,事务和脚本(lua)三者的区别

三者都可以批量执行命令

管道无原子性，命令都是独立的，属于无状态的操作

事务和脚本是有原子性的，其区别在于脚本可借助Lua语言可在服务器端存储的便利性定制和简化操作

事务和脚本是有原子性的，其区别在于脚本可借助Lua语言可在服务器端存储的便利性定制和简化操作

## 发布与订阅

Redis提供了发布订阅功能，可以用于消息的传输

Redis的发布订阅机制包括三个部分，publisher，subscriber和Channel

发布者和订阅者都是Redis客户端，Channel则为Redis服务器端

发布者将消息发送到某个的频道，订阅了这个频道的订阅者就能接收到这条消息

### subscribe:订阅

Redis客户端1订阅频道1和频道2

```bash
127.0.0.1:6379> subscribe ch1 ch2
Reading messages... (press Ctrl-C to quit)
1) "subscribe"
2) "ch1"
3) (integer) 1
1) "subscribe"
2) "ch2"
3) (integer) 2
```

### publish:发布消息

Redis客户端2将消息发布在频道1和频道2上

```bash
127.0.0.1:6379> publish ch1 hello
(integer) 1
127.0.0.1:6379> publish ch2 world
(integer) 1
```

Redis客户端1接收到频道1和频道2的消息

```bash
1) "message"
2) "ch1"
3) "hello"
1) "message"
2) "ch2"
3) "world"
```

### unsubscribe：退订

Redis客户端1退订频道1

```bash
127.0.0.1:6379> unsubscribe ch1
1) "unsubscribe"
2) "ch1"
3) (integer) 0
```

### psubscribe ：订阅模糊匹配

psubscribe ch*

### punsubscribe 退订模糊匹配

### 使用场景

1、在Redis哨兵模式中，哨兵通过发布与订阅的方式与Redis主服务器和Redis从服务器进行通信

2、Redisson是一个分布式锁框架，在Redisson分布式锁释放的时候，是使用发布与订阅的方式通知的

## 慢查询日志

### 慢查询设置

在redis.conf中可以配置和慢查询日志相关的选项：

```bash
#执行时间超过多少微秒的命令请求会被记录到日志上 0 :全记录 <0 不记录
slowlog-log-slower-than 10000
#slowlog-max-len 存储慢查询日志条数
slowlog-max-len 128
```

Redis使用列表存储慢查询日志，采用队列方式（FIFO）

config set的方式可以临时设置，redis重启后就无效

```bash
127.0.0.1:6379> config set slowlog-log-slower-than 0
OK
127.0.0.1:6379> config set slowlog-max-len 2
OK
127.0.0.1:6379> set name:001 zhaoyun
OK
127.0.0.1:6379> set name:002 zhangfei
OK
127.0.0.1:6379> get name:002
"zhangfei"
127.0.0.1:6379> slowlog get
1) 1) (integer) 7 #日志的唯一标识符(uid)
2) (integer) 1589774302 #命令执行时的UNIX时间戳
3) (integer) 65 #命令执行的时长(微秒)
4) 1) "get" #执行命令及参数
2) "name:002"
5) "127.0.0.1:37277"
6) ""
2) 1) (integer) 6
2) (integer) 1589774281
3) (integer) 7
4) 1) "set"
2) "name:002"
3) "zhangfei"
5) "127.0.0.1:37277"
6) ""
# set和get都记录，第一条被移除了。
```