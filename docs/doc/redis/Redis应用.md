

#  redis应用

## 生产环境不能使用 keys指令

Redis Keys 命令用于查找所有符合给定模式pattern的key,如果想查看Redis 某类型的key，不少小伙伴想到用keys命令，如:keys 前缀*。

但是，redis的`keys`是遍历匹配的，数据库数据越多就越慢。我们知道，redis是单线程的，如果数据比较多的话，keys指令就会导致redis线程阻塞，线上服务也会停顿了，直到指令执行完，服务才会恢复。因此，**一般在生产环境，不要使用keys指令**。

可以使用scan指令，它同keys命令一样提供模式匹配功能。它通过游标分步进行，**不会阻塞redis线程**;但是会有一定的重复概率，需要在客户端做一次去重,整体所花费的时间会比直接用keys指令长。

> scan支持增量式迭代命令，增量式迭代命令也是有缺点的：举个例子， 使用 SMEMBERS 命令可以返回集合键当前包含的所有元素， 但是对于 SCAN 这类增量式迭代命令来说， 因为在对键进行增量式迭代的过程中， 键可能会被修改， 所以增量式迭代命令只能对被返回的元素提供有限的保证 。

```java
        String patternKey = "pay:*";
        Set<String> keySet = (Set<String>) redisTemplate.execute((RedisCallback<Set<String>>) connection -> {
            Set<String> keysTmp = new HashSet<>();
            Cursor<byte[]> cursor = connection.scan(new ScanOptions.ScanOptionsBuilder().match(patternKey).count(1000).build());
            while (cursor.hasNext()) {
                keysTmp.add(new String(cursor.next()));
            }
            try {
                cursor.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return keysTmp;
        });
```

## 自增计数

> INCR key

```bash
127.0.0.1:0>incr test
"1"
127.0.0.1:0>incr test
"2"
```

将 `key` 中储存的数字值增一,如果 `key` 不存在，那么 `key` 的值会先被初始化为 `0` ，然后再执行 [INCR](http://redis.readthedocs.org/en/latest/string/incr.html#incr) 操作。

如果值包含错误的类型，或字符串类型的值不能表示为数字，那么返回一个错误。

本操作的值限制在 64 位(bit)有符号数字表示之内。

> 生成订单编号:6位以上自增id

```java
        StringBuilder sb = new StringBuilder();
        String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
        sb.append(date);
        String key = "order:" + date;
        Long increment = redisService.incr(key, 1);
		String incrementStr = increment.toString();
        if (incrementStr.length() <= 6) {
            sb.append(String.format("%06d", increment));
        } else {
            sb.append(incrementStr);
        }
        return sb.toString();
```

## Spring Cache 操作Redis

@EnableCaching
开启缓存功能，一般放在启动类上。

@Cacheable
使用该注解的方法当缓存存在时，会从缓存中获取数据而不执行方法，当缓存不存在时，会执行方法并把返回结果存入缓存中。一般使用在查询方法上，可以设置如下属性：

- value：缓存名称（必填），指定缓存的命名空间；
- key：用于设置在命名空间中的缓存key值，可以使用SpEL表达式定义；
- unless：条件符合则不缓存；
- condition：条件符合则缓存。

@CachePut
使用该注解的方法每次执行时都会把返回结果存入缓存中。一般使用在新增方法上，可以设置如下属性：

- value：缓存名称（必填），指定缓存的命名空间；
- key：用于设置在命名空间中的缓存key值，可以使用SpEL表达式定义；
- unless：条件符合则不缓存；
- condition：条件符合则缓存。

@CacheEvict

使用该注解的方法执行时会清空指定的缓存。一般使用在更新或删除方法上，可以设置如下属性：

- value：缓存名称（必填），指定缓存的命名空间；
- key：用于设置在命名空间中的缓存key值，可以使用SpEL表达式定义；
- condition：条件符合则缓存。

```yaml
<!--redis依赖配置-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```

在启动类上添加@EnableCaching注解启动缓存功能

```java
@EnableCaching
@SpringBootApplication
public class DemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
```

```java
/**
 * @author LH
 * @since 2021-03-25 14:57:49
 */
@Service("pmsProductService")
public class PmsProductServiceImpl implements PmsProductService {
    @Resource
    private PmsProductDao pmsProductDao;

    @Override
    public PmsProduct insert(PmsProduct pmsProduct) {
        this.pmsProductDao.insert(pmsProduct);
        return pmsProduct;
    }
    
    @Override
    @Cacheable(value = "test", key = "'pms:product:'+#id", unless = "#result==null")
    public PmsProduct queryById(Long id) {
        return this.pmsProductDao.queryById(id);
    }

    @Override
    @CacheEvict(value = "test",key = "'pms:product:'+#pmsProduct.id")
    public PmsProduct update(PmsProduct pmsProduct) {
        this.pmsProductDao.update(pmsProduct);
        return this.queryById(pmsProduct.getId());
    }

    @Override
    @CacheEvict(value = "test",key = "'pms:product:'+#id")
    public boolean deleteById(Long id) {
        return this.pmsProductDao.deleteById(id) > 0;
    }
}
```

![20210326-152759-0687.png](https://gitee.com/yugutou_l/pic/raw/master/image/20210326-152759-0687.png)

>让Redis中存储的数据变成标准的JSON格式，然后可以设置一定的过期时间，不设置过期时间容易产生很多不必要的缓存数据。

```java
/**
 * @author: LH
 * @version: 2021/3/25
 */
@Configuration
public class BaseRedisConfig {

    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisSerializer<Object> serializer = redisSerializer();
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(serializer);
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashValueSerializer(serializer);
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }

    @Bean
    public RedisSerializer<Object> redisSerializer() {
        //创建JSON序列化器
        Jackson2JsonRedisSerializer<Object> serializer = new Jackson2JsonRedisSerializer<>(Object.class);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        //必须设置，否则无法将JSON转化为对象，会转化成Map类型
        objectMapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance,ObjectMapper.DefaultTyping.NON_FINAL);
        serializer.setObjectMapper(objectMapper);
        return serializer;
    }

    @Bean
    public RedisCacheManager redisCacheManager(RedisConnectionFactory redisConnectionFactory) {
        RedisCacheWriter redisCacheWriter = RedisCacheWriter.nonLockingRedisCacheWriter(redisConnectionFactory);
        //设置Redis缓存有效期为1天
        RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(redisSerializer())).entryTtl(Duration.ofDays(1));
        return new RedisCacheManager(redisCacheWriter, redisCacheConfiguration);
    }
}
```

![20210326-161101-0714.png](./image/20210326-161101-0714.png)