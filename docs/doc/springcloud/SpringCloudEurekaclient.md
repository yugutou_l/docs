# SpringCloud Eureka client

## 服务注册的入口

EurekaClientAutoConfiguration

```java
public class EurekaClientAutoConfiguration {

    @Bean
	public EurekaServiceRegistry eurekaServiceRegistry() {
		return new EurekaServiceRegistry();
	}
    
    @Bean
    @ConditionalOnBean(AutoServiceRegistrationProperties.class)
    @ConditionalOnProperty(
          value = "spring.cloud.service-registry.auto-registration.enabled",
          matchIfMissing = true)
    public EurekaAutoServiceRegistration eurekaAutoServiceRegistration(
          ApplicationContext context, EurekaServiceRegistry registry,
          EurekaRegistration registration) {
       return new EurekaAutoServiceRegistration(context, registry, registration);
    }
    
    @Bean(destroyMethod = "shutdown")
    @ConditionalOnMissingBean(value = EurekaClient.class,
                              search = SearchStrategy.CURRENT)
    @org.springframework.cloud.context.config.annotation.RefreshScope
    @Lazy
    public EurekaClient eurekaClient(ApplicationInfoManager manager,
                 EurekaClientConfig config, EurekaInstanceConfig instance,
                 @Autowired(required = false) HealthCheckHandler healthCheckHandler) {
        ApplicationInfoManager appManager;
        if (AopUtils.isAopProxy(manager)) {
            appManager = ProxyUtils.getTargetObject(manager);
        }
        else {
            appManager = manager;
        }
        CloudEurekaClient cloudEurekaClient = new CloudEurekaClient(appManager,
                                                 config, this.optionalArgs, this.context);
        cloudEurekaClient.registerHealthCheck(healthCheckHandler);
        return cloudEurekaClient;
    }
    
    @Bean
    @org.springframework.cloud.context.config.annotation.RefreshScope
    @ConditionalOnBean(AutoServiceRegistrationProperties.class)
    @ConditionalOnProperty(
        value = "spring.cloud.service-registry.auto-registration.enabled",matchIfMissing = true)
    public EurekaRegistration eurekaRegistration(EurekaClient eurekaClient,
           CloudEurekaInstanceConfig instanceConfig,
           ApplicationInfoManager applicationInfoManager, 
           @Autowired(required = false) ObjectProvider<HealthCheckHandler> healthCheckHandler) {
        return EurekaRegistration.builder(instanceConfig).with(applicationInfoManager)
            .with(eurekaClient).with(healthCheckHandler).build();
    }
}
```

EurekaAutoServiceRegistration

```java
public class EurekaAutoServiceRegistration implements AutoServiceRegistration,
      SmartLifecycle, Ordered, SmartApplicationListener {
    //实现SmartLifecycle接口，spring上下文refresh中最后一步finishRefresh()中执行
    @Override
	public void start() {
		if (!this.running.get() && this.registration.getNonSecurePort() > 0) {
			//注册
			this.serviceRegistry.register(this.registration);
			this.context.publishEvent(new InstanceRegisteredEvent<>(this,
					this.registration.getInstanceConfig()));
			this.running.set(true);
		}
	}   
}
```

EurekaServiceRegistry#register#maybeInitializeClient

```
private void maybeInitializeClient(EurekaRegistration reg) {
   reg.getApplicationInfoManager().getInfo();
   //初始化EurekaClient
   reg.getEurekaClient().getApplications();
}
```

DiscoveryClient

```java
DiscoveryClient(ApplicationInfoManager applicationInfoManager, EurekaClientConfig config, AbstractDiscoveryClientOptionalArgs args,
                    Provider<BackupRegistry> backupRegistryProvider, EndpointRandomizer endpointRandomizer){
    ......
    initScheduledTasks();
	......
}
```

DiscoveryClient#initScheduledTasks

```java
private void initScheduledTasks() {
    .....
    //定时任务拉取注册信息
    cacheRefreshTask = new TimedSupervisorTask(
        "cacheRefresh",
        scheduler,
        cacheRefreshExecutor,
        registryFetchIntervalSeconds,
        TimeUnit.SECONDS,
        expBackOffBound,
        new CacheRefreshThread()
    );
    ....
    //定时心跳任务
    heartbeatTask = new TimedSupervisorTask(
        "heartbeat",
        scheduler,
        heartbeatExecutor,
        renewalIntervalInSecs,
        TimeUnit.SECONDS,
        expBackOffBound,
        new HeartbeatThread()
    );
}
```

CacheRefreshThread

```java
class CacheRefreshThread implements Runnable {
    public void run() {
        //拉取注册信息
        refreshRegistry();
    }
}
```

HeartbeatThread

```java
private class HeartbeatThread implements Runnable {
    public void run() {
        //心跳任务
        if (renew()) {
            lastSuccessfulHeartbeatTimestamp = System.currentTimeMillis();
        }
    }
}
```

![20220410-193903-0819.jpg](./image/20220410-193903-0819.jpg)
