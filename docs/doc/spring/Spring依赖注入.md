# Spring依赖注入

## @Autowired注解的自动注入

@Autowired注入是先byType，如果找到多个则byName

@Autowired注解可以写在：

1. 属性上：先根据属性类型去找Bean，如果找到多个再根据属性名确定一个
2. 构造方法上：先根据方法参数类型去找Bean，如果找到多个再根据参数名确定一个
3. set方法上：先根据方法参数类型去找Bean，如果找到多个再根据参数名确定一个

> 实现原理

### 找出注入点

在创建一个Bean的过程中，AutowiredAnnotationBeanPostProcessor#postProcessMergedBeanDefinition()找出注入点，加了@Autowired注解的某个属性、某个方法（先不管构造方法）就是注入点

把这些注入点信息找出来之后会进行缓存中（Set<InjectedElement>），InjectedElement就表示注入点

### 进行注入

AutowiredAnnotationBeanPostProcessor#postProcessProperties()，会遍历Set<InjectedElement>中的注入点开始进行注入

![20220322-124000-0815](./image/20220322-124000-0815.png)

## resolveDependency

### DefaultListableBeanFactory#resolveDependency()

![20220322-125214-0899](./image/20220322-125214-0899.png)

### DefaultListableBeanFactory#findAutowireCandidates()

1. 找出BeanFactory中类型为type的所有的Bean的名字，注意是名字，而不是Bean对象，因为我们可以根据BeanDefinition就能判断和当前type是不是匹配
2. 把resolvableDependencies中key为type的对象找出来并添加到result中
3. 遍历根据type找出的beanName，判断当前beanName对应的Bean是不是能够被自动注入
4. 先判断beanName对应的BeanDefinition中的autowireCandidate属性，如果为false，表示不能用来进行自动注入，如果为true则继续进行判断
5. 判断当前type是不是泛型，如果是泛型是会把容器中所有的beanName找出来的，如果是这种情况，那么在这一步中就要获取到泛型的真正类型，然后进行匹配，如果当前beanName和当前泛型对应的真实类型匹配，那么则继续判断
6. 如果当前DependencyDescriptor上存在@Qualifier注解，那么则要判断当前beanName上是否定义了Qualifier，并且是否和当前DependencyDescriptor上的Qualifier相等，相等则匹配
7. 经过上述验证之后，当前beanName才能成为一个可注入的，添加到result中

![20220322-125918-0307](./image/20220322-125918-0307.png)