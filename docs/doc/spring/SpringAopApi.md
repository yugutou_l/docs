# Spring Aop Api

## Joinpoint

> 接入点接口

Interceptor 执行上下文 — Invocation

方法拦截器执行上下文 — MethodInvocation

MethodInvocation执行，即被代理方法的执行

MethodInvocation实现：

- 基于反射 — ReflectiveMethodInvocation
- 基于CGLIB — CglibMethodInvocation

## Pointcut

> Joinpoint条件接口-Pointcut

核心组件

- 类过滤器 — ClassFilter
- 方法匹配器 — MethodMatcher

```java
public class EchoServicePointcut extends StaticMethodMatcherPointcut {

    private String methodName;

    public EchoServicePointcut(String methodName) {
        this.methodName = methodName;
    }

    @Override
    public boolean matches(Method method, Class<?> targetClass) {
        return method.getName().equals(methodName);
    }
}
```

## Advice

> Joinpoint执行动作接口-Advice

- 环绕动作 — Intercepter
- 前置动作 — BeforeAdvice
- 后置动作 — AfterAdvice、AfterReturningAdvice、ThrowsAdvice

## Adviosr

> Advice容器接口-Advisor （一对一关系）

## PointcutAdvisor

> Pointcut与Advice连接器-PointcutAdvisor

- 通用实现 DefaultPointcutAdvisor
- AspectJ实现 AspectJExpressionPointcutAdvisor  AspectJPointcutAdvisor
- 静态方法实现 StaticMethodMatcherPointcutAdvisor
- Ioc容器实现 AbstractBeanFactoryPointcutAdvisor

## AdvisorAdapter

> Advisor的Interceptor适配器（将adivce适配成Intercepter）

org.springframework.aop.framework.adapter.MethodBeforeAdviceAdapter

```java
class MethodBeforeAdviceAdapter implements AdvisorAdapter, Serializable {

   @Override
   public boolean supportsAdvice(Advice advice) {
      return (advice instanceof MethodBeforeAdvice);
   }

   @Override
   public MethodInterceptor getInterceptor(Advisor advisor) {
      MethodBeforeAdvice advice = (MethodBeforeAdvice) advisor.getAdvice();
      return new MethodBeforeAdviceInterceptor(advice);
   }
}
```

## AopProxy

> AOP代理接口

- JDK 动态代理    JdkDynamicAopProxy
- CGLIB 字节码提升    ObjenesisCglibAopProxy

## AopProxyFactory

> AopProxy工厂接口

默认实现: DefaultAopProxyFactory

```java
public class DefaultAopProxyFactory implements AopProxyFactory, Serializable {
	@Override
	public AopProxy createAopProxy(AdvisedSupport config) throws AopConfigException {
		if (config.isOptimize() || config.isProxyTargetClass() || hasNoUserSuppliedProxyInterfaces(config)) {
			Class<?> targetClass = config.getTargetClass();
			if (targetClass == null) {
				throw new AopConfigException("TargetSource cannot determine target class: " +
						"Either an interface or a target is required for proxy creation.");
			}
			if (targetClass.isInterface() || Proxy.isProxyClass(targetClass)) {
				return new JdkDynamicAopProxy(config);
			}
			return new ObjenesisCglibAopProxy(config);
		}
		else {
			return new JdkDynamicAopProxy(config);
		}
	}
	private boolean hasNoUserSuppliedProxyInterfaces(AdvisedSupport config) {
		Class<?>[] ifcs = config.getProxiedInterfaces();
		return (ifcs.length == 0 || (ifcs.length == 1 && SpringProxy.class.isAssignableFrom(ifcs[0])));
	}

}
```

## AdvisedSupport

> AopProxyFactory 代理配置管理器

基类: ProxyConfig

实现接口: Advised       
