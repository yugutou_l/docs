# Spring BeanDefinition

## BeanDefinition

beanDefinition是Spring Framework中定义Bean的配置元信息接口，包含：

- Bean的类名
- Bean行为配置元素，如作用域、自动绑定模式、生命周期回调等
- 其他Bean引用，又可称合作者或依赖
- 配置设置，如Bean属性

bean实例化，需要先注册对应beanDefinition。我们通过@Component，@Bean等方式所定义的Bean，首先会被解析为BeanDefinition对象，然后通过beanDefinition进行实例化

> BeanDefinition 元信息

| 属性                     | 说明                                       |
| ------------------------ | ------------------------------------------ |
| Class                    | Bean 全类名，不能是抽象类或接口            |
| Name                     | Bean 的名称或ID                            |
| Scope                    | Bean 的作用域 （如：singleton、prototype） |
| Constructor argument     | Bean 构造参数 （用于依赖注入）             |
| Properties               | Bean 属性设置 （用于依赖注入）             |
| Autowing mode            | Bean 自动绑定模式                          |
| Lazy initialization mode | Bean 延迟初始化模式                        |
| Initialization method    | Bean 初始化回调方法名称                    |
| Destruction method       | Bean 销毁回调方法名称                      |

```java
public class BeanDefinitionCreationDemo {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.refresh();
        // 1.通过 BeanDefinitionBuilder 构建
        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(User.class);
        // 通过属性设置
        beanDefinitionBuilder
                .addPropertyValue("id", 1)
                .addPropertyValue("name", "鱼骨头");
        // 获取 BeanDefinition 实例
        BeanDefinition beanDefinition = beanDefinitionBuilder.getBeanDefinition();
        context.registerBeanDefinition("user1",beanDefinition);
        User user1 = context.getBean("user1", User.class);

        // 2. 通过 AbstractBeanDefinition 以及派生类
        GenericBeanDefinition genericBeanDefinition = new GenericBeanDefinition();
        // 设置 Bean 类型
        genericBeanDefinition.setBeanClass(User.class);
        MutablePropertyValues propertyValues = new MutablePropertyValues();
        propertyValues
                .add("id", 2)
                .add("name", "小余儿");
        genericBeanDefinition.setPropertyValues(propertyValues);
        context.registerBeanDefinition("user2",genericBeanDefinition);
        User user2 = context.getBean("user2", User.class);
    }
}
```

### AbstractBeanDefinition

AbstractBeanDefinition的三个子类：

#### GenericBeanDefinition:

替代了原来的`ChildBeanDefinition`，比起`ChildBeanDefinition`更为灵活，`ChildBeanDefinition`在实例化的时候必须要指定一个`parentName`,而`GenericBeanDefinition`不需要。我们通过注解配置的bean以及我们的配置类（除`@Bena`外）的`BeanDefiniton`类型都是`GenericBeanDefinition`。

#### ChildBeanDefinition：

现在已经被`GenericBeanDefinition`所替代了。

#### RootBeanDefinition

- Spring在启动时会实例化几个初始化的`BeanDefinition`,这几个`BeanDefinition`的类型都为`RootBeanDefinition`
- AbstractBeanFactory#getMergedLocalBeanDefinition，将beanDefinition合并转化为RootBeanDefinition，然后进行bean实例化
- 我们通过`@Bean`注解配置的bean，解析出来的`BeanDefinition`都是`RootBeanDefinition`（实际上是其子类`ConfigurationClassBeanDefinition`）

### AnnotatedBeanDefinition

这个接口继承了我们的`BeanDefinition`接口，我们查看其源码可以发现：

```java
AnnotationMetadata getMetadata();

@Nullable
MethodMetadata getFactoryMethodMetadata();
```

这个接口相比于`BeanDefinition`， 仅仅多提供了两个方法

- `getMetadata()`,主要用于获取注解元素据。从接口的命名上我们也能看出，这类主要用于保存通过注解方式定义的bean所对应的`BeanDefinition`。所以它多提供了一个关于获取注解信息的方法
- `getFactoryMethodMetadata()`,`@Bean`注解生成的BeanDefinition，被`@Bean`注解标记的方法，就被解析成了`FactoryMethodMetadata`。

AnnotatedBeanDefinition的三个实现类：

#### AnnotatedGenericBeanDefinition:

- 通过形如下面的API注册的bean都是`AnnotatedGenericBeanDefinition`

```java
public static void main(String[] args) {
    AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext();
    ac.register(Config.class);
}
```

这里的`config`对象，最后在Spring容器中就是一个`AnnotatedGenericBeanDefinition`。

通过`@Import`注解导入的类，最后都是解析为`AnnotatedGenericBeanDefinition`。

#### ScannedGenericBeanDefinition:

被扫描注解的类，如`@Service`,`@Compent`等方式配置的Bean都是`ScannedGenericBeanDefinition`

#### ConfigurationClassBeanDefinition:

通过`@Bean`的方式配置的Bean为`ConfigurationClassBeanDefinition`

## BeanDefinitionReader

BeanDefinitionReader分为几类：

### AnnotatedBeanDefinitionReader

可以直接把某个类转换为BeanDefinition，并且会解析该类上的注解

它能解析的注解是：@Conditional，@Scope、@Lazy、@Primary、@DependsOn、@Role、@Description

```java
DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
AnnotatedBeanDefinitionReader annotatedBeanDefinitionReader = new AnnotatedBeanDefinitionReader(beanFactory);
// 将User.class解析为BeanDefinition
annotatedBeanDefinitionReader.register(User.class);
System.out.println(beanFactory.getBean("user"));
```

### XmlBeanDefinitionReader

可以解析<bean/>标签

```java
XmlBeanDefinitionReader xmlBeanDefinitionReader = new XmlBeanDefinitionReader(beanFactory);
int i = xmlBeanDefinitionReader.loadBeanDefinitions("spring.xml");
System.out.println(beanFactory.getBean("user"));
```

### **ClassPathBeanDefinitionScanner**

这个并不是BeanDefinitionReader，但是它的作用和BeanDefinitionReader类似，它可以进行扫描，扫描某个包路径，对扫描到的类进行解析，比如，扫描到的类上如果存在@Component注解，那么就会把这个类解析为一个BeanDefinition

```java
AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
ClassPathBeanDefinitionScanner myScanner=new ClassPathBeanDefinitionScanner(context,false);
myScanner.addIncludeFilter(new AnnotationTypeFilter(MyAnn.class));
int scan = myScanner.scan("thinking.in.spring");
System.out.println(scan);
//ClassPathBeanDefinitionScanner父类
ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false)     {
        @Override
        protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
            return beanDefinition.getMetadata().isInterface();
        }
    };
scanner.addIncludeFilter(new AnnotationTypeFilter(MyAnn.class));
Set<BeanDefinition> definitions = scanner.findCandidateComponents("thinking.in.spring");
for (BeanDefinition definition : definitions) {
    GenericBeanDefinition genericBeanDefinition = (GenericBeanDefinition) definition;
    String className = genericBeanDefinition.getBeanClassName();
    definition.setBeanClassName("thinking.in.spring.MyProxy");
    definition.getConstructorArgumentValues().addGenericArgumentValue(className);
    context.registerBeanDefinition(className,definition);
}
```

