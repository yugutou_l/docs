# Spring 事件监听器

## Spring 内建事件

ApplicationContextEvent 派生事件

- ContextRefreshedEvent ：Spring 应用上下文就绪事件
- ContextStartedEvent ：Spring 应用上下文启动事件
- ContextStoppedEvent ：Spring 应用上下文停止事件
- ContextClosedEvent ：Spring 应用上下文关闭事件

> 默认的发布器为`SimpleApplicationEventMulticaster`
> AbstractApplicationContext#initApplicationEventMulticaster

```java
public class ApplicationListenerDemo {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(ApplicationListenerDemo.class);
        //方法一：基于 ConfigurableApplicationContext API 实现
        context.addApplicationListener(new ApplicationListener<ContextRefreshedEvent>() {
            @Override
            public void onApplicationEvent(ContextRefreshedEvent event) {
                System.out.println("ApplicationListener - 接收到 Spring 事件：" + event);
            }
        });
        //方法二：注册为 Spring Bean

        //方法三：基于 Spring 注解：@EventListener

        context.refresh();

        // 依赖查找 ApplicationEventMulticaster
        ApplicationEventMulticaster applicationEventMulticaster =
                context.getBean(AbstractApplicationContext.APPLICATION_EVENT_MULTICASTER_BEAN_NAME, ApplicationEventMulticaster.class);
        // 判断当前 ApplicationEventMulticaster 是否为 SimpleApplicationEventMulticaster
        if (applicationEventMulticaster instanceof SimpleApplicationEventMulticaster) {
            SimpleApplicationEventMulticaster simpleApplicationEventMulticaster =
                    (SimpleApplicationEventMulticaster) applicationEventMulticaster;
            // 切换 taskExecutor
            ExecutorService taskExecutor = newSingleThreadExecutor(new CustomizableThreadFactory("my-spring-event-thread-pool"));
            // 同步 -> 异步
            simpleApplicationEventMulticaster.setTaskExecutor(taskExecutor);

            // 添加 ContextClosedEvent 事件处理
            applicationEventMulticaster.addApplicationListener(new ApplicationListener<ContextClosedEvent>() {
                @Override
                public void onApplicationEvent(ContextClosedEvent event) {
                    if (!taskExecutor.isShutdown()) {
                        taskExecutor.shutdown();
                    }
                }
            });

            simpleApplicationEventMulticaster.setErrorHandler(e -> {
                System.err.println("当 Spring 事件异常时，原因：" + e.getMessage());
            });
        }
        context.publishEvent(new MyApplicationEvent(ApplicationListenerDemo.class,new User(1L,"鱼骨头")));
        applicationEventMulticaster.multicastEvent(new MyApplicationEvent(ApplicationListenerDemo.class,new User(2L,"小余儿")));
        context.close();
    }


    static class MyApplicationEvent extends ApplicationEvent{
        private User user;

        public MyApplicationEvent(Object source,User user) {
            super(source);
            this.user=user;
        }

        public User getUser() {
            return user;
        }
    }

    static class MyApplicationListener implements ApplicationListener<MyApplicationEvent>{

        @Override
        public void onApplicationEvent(MyApplicationEvent event) {
            System.out.println("MyApplicationListener - 接收到接收到事件：" + event+"user:"+event.getUser());
        }
    }

    @Bean
    public MyApplicationListener myPayloadApplicationListener(){
        return new MyApplicationListener();
    }

    @EventListener
    public void onMyApplicationEvent(MyApplicationEvent event){
        throw new RuntimeException("故意抛出异常");
    }
}
```

