# Spring Aop  代理生成

## 手动方式

### ProxyFactory

```java
public class DefaultEchoService implements EchoService {
    @Override
    public String echo(String message) {
        return "[ECHO] " + message;
    }
}
```

```java
public class EchoServiceMethodInterceptor implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Method method = invocation.getMethod();
        System.out.println("拦截 EchoService 的方法：" + method);
        return invocation.proceed();
    }
}
```

```java
public class ProxyFactoryDemo {
    public static void main(String[] args) {
        DefaultEchoService defaultEchoService = new DefaultEchoService();
        // 注入目标对象（被代理）
        ProxyFactory proxyFactory = new ProxyFactory(defaultEchoService);
        // 添加 Advice 实现 MethodInterceptor < Interceptor < Advice
        proxyFactory.addAdvice(new EchoServiceMethodInterceptor());
        // 获取代理对象
        EchoService echoService = (EchoService) proxyFactory.getProxy();
        System.out.println(echoService.echo("Hello,World"));
    }
}
```

```bash
拦截 EchoService 的方法：public abstract java.lang.String org.geekbang.thinking.in.spring.aop.overview.EchoService.echo(java.lang.String)
[ECHO] Hello,World
```

ProxyFactory就是一个代理对象生产工厂，在生成代理对象之前需要对代理工厂进行配置。

ProxyFactory在生成代理对象之前需要决定到底是使用JDK动态代理还是CGLIB技术：

```java
// config就是ProxyFactory对象

// optimize为true,或proxyTargetClass为true,或用户没有给ProxyFactory对象添加interface
if (config.isOptimize() || config.isProxyTargetClass() || hasNoUserSuppliedProxyInterfaces(config)) {
	Class<?> targetClass = config.getTargetClass();
	if (targetClass == null) {
		throw new AopConfigException("TargetSource cannot determine target class: " +
				"Either an interface or a target is required for proxy creation.");
	}
    // targetClass是接口，直接使用Jdk动态代理
	if (targetClass.isInterface() || Proxy.isProxyClass(targetClass)) {
		return new JdkDynamicAopProxy(config);
	}
    // 使用Cglib
	return new ObjenesisCglibAopProxy(config);
}
else {
    // 使用Jdk动态代理
	return new JdkDynamicAopProxy(config);
}
```

JdkDynamicAopProxy创建代理对象过程

1. 获取生成代理对象所需要实现的接口集合

1. 1. 获取通过ProxyFactory.addInterface()所添加的接口，如果没有通过ProxyFactory.addInterface()添加接口，那么则看ProxyFactory.setTargetClass()所设置的targetClass是不是一个接口，把接口添加到结果集合中
   2. 同时把SpringProxy、Advised、DecoratingProxy这几个接口也添加到结果集合中去

2. 确定好要代理的集合之后，就利用Proxy.newProxyInstance()生成一个代理对象



JdkDynamicAopProxy创建的代理对象执行过程

1. 如果通过ProxyFactory.setExposeProxy()把exposeProxy设置为了true，那么则把代理对象设置到一个ThreadLocal（currentProxy）中去。
2. 获取通过ProxyFactory所设置的target，如果设置的是targetClass，那么target将为null
3. 根据当前所调用的方法对象寻找ProxyFactory中所添加的并匹配的Advisor，并且把Advisor封装为MethodInterceptor返回，得到MethodInterceptor链叫做chain
4. 如果chain为空，则直接执行target对应的当前方法，如果target为null会报错
5. 如果chain不为空，则会依次执行chain中的MethodInterceptor

1. 1. 如果当前MethodInterceptor是MethodBeforeAdviceInterceptor，那么则先执行Advisor中所advice的before()方法，然后执行下一个MethodInterceptor
   2. 如果当前MethodInterceptor是AfterReturningAdviceInterceptor，那么则先执行下一个MethodInterceptor，拿到返回值之后，再执行Advisor中所advice的afterReturning()方法



ObjenesisCglibAopProxy创建代理对象过程

1. 创建Enhancer
2. 设置Enhancer的superClass为通过ProxyFactory.setTarget()所设置的对象的类
3. 设置Enhancer的interfaces为通过ProxyFactory.addInterface()所添加的接口，以及SpringProxy、Advised接口
4. 设置Enhancer的Callbacks为DynamicAdvisedInterceptor
5. 最后通过Enhancer创建一个代理对象



ObjenesisCglibAopProxy创建的代理对象执行过程

执行过程为DynamicAdvisedInterceptor中的实现，执行逻辑和JdkDynamicAopProxy中是一样的。



### ProxyFactoryBean

```java
public class ProxyFactoryBeanDemo {

    @Bean
    public EchoService echoService(){
        return new DefaultEchoService();
    }
    
    @Bean
    public EchoServiceMethodInterceptor echoServiceMethodInterceptor(){
        return new EchoServiceMethodInterceptor();
    }

    @Bean
    public ProxyFactoryBean echoServiceProxyFactoryBean(EchoService echoService){
        ProxyFactoryBean proxyFactoryBean = new ProxyFactoryBean();
        proxyFactoryBean.setTarget(echoService);
        proxyFactoryBean.setInterceptorNames("echoServiceMethodInterceptor");
        return proxyFactoryBean;
    }

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ProxyFactoryBeanDemo.class);
        EchoService echoService = context.getBean("echoServiceProxyFactoryBean", EchoService.class);
        System.out.println(echoService.echo("Hello,World"));
        context.close();
    }
}
```

```bash
拦截 EchoService 的方法：public abstract java.lang.String org.geekbang.thinking.in.spring.aop.overview.EchoService.echo(java.lang.String)
[ECHO] Hello,World
```

### AspectJProxyFactory

```java
@Aspect
public class AspectConfiguration2 implements Ordered {
    
    @Before("execution(public * *(..))")          // Join Point 拦截动作
    public void beforeAnyPublicMethod2() {
        System.out.println("@Before any public method.(2)");
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
```



```java
public class AspectJAnnotationUsingAPIDemo {
    public static void main(String[] args) {
        DefaultEchoService defaultEchoService = new DefaultEchoService();
        AspectJProxyFactory proxyFactory = new AspectJProxyFactory(defaultEchoService);
        proxyFactory.addAspect(AspectConfiguration2.class);
        EchoService echoService = (EchoService)proxyFactory.getProxy();
        System.out.println(echoService.echo("Hello,World"));
   }
}
```

```bash
@Before any public method.(2)
[ECHO] Hello,World
```

## 自动方式

"自动代理"表示，只需要在Spring中添加某个Bean，这个Bean是一个BeanPostProcessor，那么Spring在每创建一个Bean时，都会经过这个BeanPostProcessor的判断，去判断当前正在创建的这个Bean是不是需要进行AOP。

我们可以在项目中定义很多个Advisor，定义方式有两种：

1. 通过实现PointcutAdvisor接口
2. 通过@Aspect、@Pointcut、@Before等注解

在创建某个Bean时，会根据当前这个Bean的信息，比如对应的类，以及当前Bean中的方法信息，去和定义的所有Advisor进行匹配，如果匹配到了其中某些Advisor，那么就会把这些Advisor给找出来，并且添加到ProxyFactory中去，在利用ProxyFactory去生成代理对象

### AbstractAutoProxyCreator

默认实现：DefaultAdvisorAutoProxyCreator

Bean名称匹配实现：BeanNameAutoProxyCreator

AspectJ实现：AspectJAwareAdvisorAutoProxyCreator

```java
public class EchoServiceAdvisor extends StaticMethodMatcherPointcutAdvisor {

    public EchoServiceAdvisor() {
        setAdvice(new MethodBeforeAdvice(){
            @Override
            public void before(Method method, Object[] args, Object target){
                System.out.println("before");
            }
        });
    }
    @Override
    public boolean matches(Method method, Class<?> targetClass) {
        if(EchoService.class.isAssignableFrom(targetClass)){
            return true;
        }
        return false;
    }
}
```

```java
public class AutoProxyDemo {

    @Bean
    public EchoService echoService(){
        return new DefaultEchoService();
    }

    @Bean
    public EchoServiceAdvisor echoServiceAdvisor(){
        return new EchoServiceAdvisor();
    }

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(AutoProxyDemo.class,DefaultAdvisorAutoProxyCreator.class);
        context.refresh();
        EchoService echoService = context.getBean("echoService", EchoService.class);
        System.out.println(echoService.echo("Hello,World"));
        context.close();
    }
}
```

```bash
before
[ECHO] Hello,World
```

```java
@EnableAspectJAutoProxy // 激活 Aspect 注解自动代理
public class AspectJAnnotatedPointcutDemo {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(AspectJAnnotatedPointcutDemo.class,
                AspectConfiguration2.class);
        context.refresh();
        AspectJAnnotatedPointcutDemo aspectJAnnotationDemo = context.getBean(AspectJAnnotatedPointcutDemo.class);
        aspectJAnnotationDemo.execute();
        context.close();
    }

    public void execute() {
        System.out.println("execute()...");
    }
}
```

```bash
@Before any public method.(2)
execute()...
```

## AopInfrastructureBean 

标记接口，被标记的类不需要被代理

语义：Spring Aop 基础Bean

AbstractAutoProxyCreator#isInfrastructureClass

```java
	protected boolean isInfrastructureClass(Class<?> beanClass) {
		boolean retVal = Advice.class.isAssignableFrom(beanClass) ||
				Pointcut.class.isAssignableFrom(beanClass) ||
				Advisor.class.isAssignableFrom(beanClass) ||
				AopInfrastructureBean.class.isAssignableFrom(beanClass);
		if (retVal && logger.isTraceEnabled()) {
			logger.trace("Did not attempt to auto-proxy infrastructure class [" + beanClass.getName() + "]");
		}
		return retVal;
	}
```