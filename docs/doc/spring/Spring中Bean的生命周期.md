# Spring中Bean的生命周期

Bean的生命周期就是指：**在Spring中，一个Bean是如何生成的，如何销毁的**

![20220321-205355-0034.png](./image/20220321-205355-0034.png)

## Bean的生成过程

### 1. 生成BeanDefinition

解析@ComponentScan的类时候会进行扫描，调用ClassPathScanningCandidateComponentProvider#scanCandidateComponents(String basePackage)

```java
Resource[] resources = getResourcePatternResolver().getResources(packageSearchPath);

MetadataReader metadataReader = getMetadataReaderFactory().getMetadataReader(resource);
```

拿到所指定的包路径下的所有文件资源（******.class文件）

然后会遍历每个Resource，利用**ASM**技术解析class文件，得到类的元数据集信息合注解信息，为每个Resource生成一个MetadataReader对象，这个对象拥有三个功能：

1. 获取对应的Resource资源
2. 获取Resource对应的class的元数据信息，包括类的名字、是不是接口、是不是一个注解、是不是抽象类、有没有父类，父类的名字，所实现的所有接口的名字，内部类的类名等等。
3. 获取Resource对应的class上的注解信息，当前类上有哪些注解，当前类中有哪些方法上有注解

有了MetadataReader对象，就相当于有了当前类的所有信息，但是当前类并没有加载，真正在用到这个类的时候才加载。



然后利用MetadataReader对象生成一个ScannedGenericBeanDefinition对象，**注意此时的BeanDefinition对象中的beanClass属性存储的是当前类的名字，而不是class对象**。（beanClass属性的类型是Object，它即可以存储类的名字，也可以存储class对象）

### 2. 合并BeanDefinition

如果某个BeanDefinition存在父BeanDefinition，那么则要进行合并，生成RootBeanDefinition

### 3. 加载类

有了RootBeanDefinition之后，后续就会基于BeanDefinition去创建Bean，而创建Bean就必须实例化对象，而实例化就必须先加载当前BeanDefinition所对应的class，在AbstractAutowireCapableBeanFactory类的createBean()方法中，一开始就会调用：

```java
Class<?> resolvedClass = resolveBeanClass(mbd, beanName);
```

如果beanClass属性的类型是Class，那么就直接返回，如果不是，则会根据类名进行加载

会利用BeanFactory所设置的类加载器来加载类，如果没有设置，则默认使用**ClassUtils.getDefaultClassLoader()**所返回的类加载器来加载。

#### **ClassUtils.getDefaultClassLoader()**

1. 优先获取当前线程中的ClassLoader
2. 如果为空，则获取加载ClassUtils类的类加载器（正常情况下，就是AppClassLoader，但是如果是在Tomcat中运行，那么则会是Tomcat中为每个应用所创建的WebappClassLoader）
3. 如果为空，那么则是bootstrap类加载器加载的ClassUtils类，那则获取系统类加载器进行加载

### 4. 实例化前

允许第三方可以不按照Spring的正常流程来创建一个Bean，可以利用InstantiationAwareBeanPostProcessor的postProcessBeforeInstantiation方法来提前返回一个Bean对象，直接结束Bean的生命周期

### 5. 推断构造方法

1. AbstractAutowireCapableBeanFactory类中的createBeanInstance()方法会去创建一个Bean实例
2. 根据BeanDefinition加载类得到Class对象
3. 如果BeanDefinition绑定了一个Supplier，那就调用Supplier的get方法得到一个对象并直接返回
4. 如果BeanDefinition中存在**factoryMethodName**，那么就**调用该工厂方法**得到一个bean对象并返回
5. 如果BeanDefinition已经自动构造过了，那就调用autowireConstructor()自动构造一个对象
6. 调用SmartInstantiationAwareBeanPostProcessor的determineCandidateConstructors()方法得到哪些构造方法是可以用的
7. 如果存在可用得构造方法，或者当前BeanDefinition的autowired是AUTOWIRE_CONSTRUCTOR，或者BeanDefinition中指定了构造方法参数值，或者创建Bean的时候指定了构造方法参数值，那么就调用**autowireConstructor()**方法自动构造一个对象
8. 最后，如果不是上述情况，就根据无参的构造方法实例化一个对象

### 6. 实例化

构造方法反射得到一个实例

### 7. BeanDefinition的后置处理

```java
for (BeanPostProcessor bp : getBeanPostProcessors()) {
	if (bp instanceof MergedBeanDefinitionPostProcessor) {
		MergedBeanDefinitionPostProcessor bdp = (MergedBeanDefinitionPostProcessor) bp;
		bdp.postProcessMergedBeanDefinition(mbd, beanType, beanName);
	}
}
```

这里可以处理BeanDefinition，此时实例对象已经生成好了，所以修改beanClass已经没用了，但是可以修改PropertyValues，比如：

```java
@Component
public class MyMergedBeanDefinitionPostProcessor implements MergedBeanDefinitionPostProcessor {
	@Override
	public void postProcessMergedBeanDefinition(RootBeanDefinition beanDefinition, Class<?> beanType, String beanName) {
		if (beanName.equals("userService")) {
			beanDefinition.getPropertyValues().add("name","小余儿");
		}
	}
}
```

AutowiredAnnotationBeanPostProcessor#postProcessMergedBeanDefinition获取依赖注入点信息

```java
public void postProcessMergedBeanDefinition(RootBeanDefinition beanDefinition, Class<?> beanType, 
String 	beanName) {
    InjectionMetadata metadata = findAutowiringMetadata(beanName, beanType, null);
    metadata.checkConfigMembers(beanDefinition);
}
```

### 8. 填充属性

调用InstantiationAwareBeanPostProcessor#postProcessProperties

AutowiredAnnotationBeanPostProcessor#postProcessProperties，获取InjectionMetadata，@Autowired会触发依赖注入

AbstractAutowireCapableBeanFactory#applyPropertyValues(beanName, mbd, bw, pvs)，处理填充属性

### 9. 执行Aware

1. ((BeanNameAware) bean).setBeanName(beanName);
2. ((BeanClassLoaderAware) bean).setBeanClassLoader(bcl);
3. ((BeanFactoryAware) bean).setBeanFactory(AbstractAutowireCapableBeanFactory.**this**);

### 10. 初始化前

```java
for (BeanPostProcessor processor : getBeanPostProcessors()) {
	Object current = processor.postProcessBeforeInitialization(result, beanName);
	if (current == null) {
		return result;
	}
	result = current;
}
```

### 11. 初始化

1. ((InitializingBean) bean).afterPropertiesSet();
2. 执行BeanDefinition中指定的初始化方法initMethod

### 12. 初始化后

```java
for (BeanPostProcessor processor : getBeanPostProcessors()) {
	Object current = processor.postProcessAfterInitialization(result, beanName);
	if (current == null) {
		return result;
	}
	result = current;
}
```

### 13. 注册销毁时的DisposableBeanAdapter

这里涉及到一个设计模式：**适配器模式**

在销毁时，Spring会找出实现了DisposableBean接口的Bean。

但是我们在定义一个Bean时，如果这个Bean实现了DisposableBean接口，或者实现了AutoCloseable接口，在BeanDefinition中指定了destroyMethodName，有@PreDestroy方法，那么这个Bean都属于“DisposableBean”，这些Bean在容器关闭时都要调用相应的销毁方法。

所以，这里就需要DisposableBeanAdapte进行适配，将需要执行销毁方法的bean适配成DisposableBean

## Bean的销毁过程

### 1. 容器关闭

### 2. 发布ContextClosedEvent事件

### 3. 调用LifecycleProcessor的onClose方法

### 4. 销毁单例Bean

1. 找出所有DisposableBean
2. 遍历每个DisposableBean
3. 找出依赖了当前DisposableBean的其他Bean，将这些Bean从单例池中移除掉
4. 调用DisposableBean的destroy()方法
5. 找到当前DisposableBean所包含的inner beans，将这些Bean从单例池中移除掉
