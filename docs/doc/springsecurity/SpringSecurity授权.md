# SpringSecurity授权

## 基于Filter

FilterSecurityInterceptor extends AbstractSecurityInterceptor

FilterSecurityInterceptor#doFilter

```java
public void doFilter(ServletRequest request, ServletResponse response,
      FilterChain chain) throws IOException, ServletException {
   FilterInvocation fi = new FilterInvocation(request, response, chain);
   invoke(fi);
}
```

FilterSecurityInterceptor#invoke

```java
public void invoke(FilterInvocation fi) throws IOException, ServletException {
   if ((fi.getRequest() != null)
         && (fi.getRequest().getAttribute(FILTER_APPLIED) != null)
         && observeOncePerRequest) {
      fi.getChain().doFilter(fi.getRequest(), fi.getResponse());
   }
   else {
      if (fi.getRequest() != null && observeOncePerRequest) {
         fi.getRequest().setAttribute(FILTER_APPLIED, Boolean.TRUE);
      }

      InterceptorStatusToken token = super.beforeInvocation(fi);
      try {
         fi.getChain().doFilter(fi.getRequest(), fi.getResponse());
      }
      finally {
         super.finallyInvocation(token);
      }

      super.afterInvocation(token, null);
   }
}
```

WebSecurityConfigurerAdapter#configure(HttpSecurity http)

```java
@Override
    protected void configure(HttpSecurity http) throws Exception {
        //配置FilterSecurityInterceptor
        http.authorizeRequests()
                .antMatchers("/showLogin","/toerror").permitAll()
                .antMatchers("/admin/index").anonymous()
                .antMatchers("/admin/demo").hasAuthority("admin")
                .anyRequest().authenticated();
    }
```

## 基于AOP

methodSecurityInterceptor extends AbstractSecurityInterceptor

```java
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig 
```

methodSecurityInterceptor#invoke

```java
public Object invoke(MethodInvocation mi) throws Throwable {
   InterceptorStatusToken token = super.beforeInvocation(mi);
   Object result;
   try {
      result = mi.proceed();
   }
   finally {
      super.finallyInvocation(token);
   }
   return super.afterInvocation(token, result);
}
```

@PreAuthorize可以用来控制一个方法是否能够被调用，执行之前先判断权限，大多情况下都是使用这个注解。

```java
//限制有admin权限
@PreAuthorize("hasAuthority('admin')")
@RequestMapping("/findById")
public User findById(long id) {
    User user = new User();
    user.setId(id);
    return user;
}


// 限制只能查询自己的信息
@PreAuthorize("principal.username.equals(#username)")
@RequestMapping("/findByName")
public User findByName(String username) {
    User user = new User();
    user.setUsername(username);
    return user;
}

//限制只能新增用户名称为abc的用户
@PreAuthorize("#user.username.equals('abc')")
@RequestMapping("/add")
public User add(User user) {
    return user;
}
```

## AbstractSecurityInterceptor

SpringSecurity鉴权逻辑在AbstractSecurityInterceptor#beforeInvocation中实现

```java
protected InterceptorStatusToken beforeInvocation(Object object) {
    .....
    //获取访问当前路径需要的权限
    Collection<ConfigAttribute> attributes = this.obtainSecurityMetadataSource()
				.getAttributes(object);
    //获取用户的验证信息
    Authentication authenticated = authenticateIfRequired();
    try {
        	//判断用户验证信息中是否包含所需权限
			this.accessDecisionManager.decide(authenticated, object, attributes);
		}
		catch (AccessDeniedException accessDeniedException) {
			publishEvent(new AuthorizationFailureEvent(object, attributes, authenticated,
					accessDeniedException));
			//无权限抛出异常
			throw accessDeniedException;
		}
}
```