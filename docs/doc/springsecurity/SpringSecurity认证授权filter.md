# SpringSecurity认证授权filter

## 认证流程 Authentication

![20220405-110503-0104.png](./image/20220405-110503-0104.png)

### UsernamePasswordAuthenticationFilter

AbstractAuthenticationProcessingFilter--doFilter方法

```java
public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) 
    throws IOException, ServletException {
.....
	Authentication authResult;
	try {
        //1.调用子类方法
        authResult = attemptAuthentication(request, response);
        ...
        //2.session策略验证
        sessionStrategy.onAuthentication(authResult, request, response);
	}catch (InternalAuthenticationServiceException failed) {
        logger.error(
            "An internal error occurred while trying to authenticate the user.",
            failed);
        unsuccessfulAuthentication(request, response, failed);
        return;
	}catch (AuthenticationException failed) {
        // 认证失败
        unsuccessfulAuthentication(request, response, failed);
        return;
		}
    ....
    // 3.成功身份验证
    successfulAuthentication(request, response, chain, authResult);
}
```

successfulAuthentication方法

```java
protected void successfulAuthentication(HttpServletRequest request,
	HttpServletResponse response, FilterChain chain, Authentication authResult)
	throws IOException, ServletException {
    ....
    // 1.将认证的用户放入SecurityContext中
    SecurityContextHolder.getContext().setAuthentication(authResult);
    // 2.检查是不是记住我
    rememberMeServices.loginSuccess(request, response, authResult);
    ...
    // 3.调用successHandler的onAuthenticationSuccess方法
    successHandler.onAuthenticationSuccess(request, response,authResult);
}

```

unsuccessfulAuthentication方法

```java
protected void unsuccessfulAuthentication(HttpServletRequest request,
      HttpServletResponse response, AuthenticationException failed)
      throws IOException, ServletException {
   SecurityContextHolder.clearContext();
   .....
   rememberMeServices.loginFail(request, response);
   // 调用failureHandler的onAuthenticationFailure方法
   failureHandler.onAuthenticationFailure(request, response, failed);
}
```

> UsernamePasswordAuthenticationFilter认证后，执行successHandler或failureHandler，不会在经过剩余的过滤器链

```java
public Authentication attemptAuthentication(HttpServletRequest request,
	HttpServletResponse response) throws AuthenticationException {
    //1.检查是否是post请求
    if (postOnly && !request.getMethod().equals("POST")) {
    throw new AuthenticationServiceException("Authentication method not supported: " +request.getMethod());
    }
    //2.获取用户名和密码
    String username = obtainUsername(request);
    String password = obtainPassword(request);
    if (username == null) {
    	username = "";
    }
    if (password == null) {
    	password = "";
    }
    username = username.trim();
    //3.创建AuthenticationToken,此时是未认证的状态
    UsernamePasswordAuthenticationToken authRequest = new
    UsernamePasswordAuthenticationToken(username, password);
    // Allow subclasses to set the "details" property
    setDetails(request, authRequest);
    //4.调用AuthenticationManager进行认证.
    return this.getAuthenticationManager().authenticate(authRequest);
}

```

UsernamePasswordAuthenticationToken

```java
public UsernamePasswordAuthenticationToken(Object principal, Object credentials) {
    super(null);
    this.principal = principal;//设置用户名
    this.credentials = credentials;//设置密码
    setAuthenticated(false);//设置认证状态为-未认证
}
```

AuthenticationManager-->ProviderManager-->AbstractUserDetailsAuthenticationProvider

```java
public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    Assert.isInstanceOf(UsernamePasswordAuthenticationToken.class,authentication,
        () -> messages.getMessage(
        "AbstractUserDetailsAuthenticationProvider.onlySupports",
        "Only UsernamePasswordAuthenticationToken is
        supported"));
    // 1.获取用户名
    String username = (authentication.getPrincipal() == null)? "NONE_PROVIDED" : authentication.getName();
    // 2.尝试从缓存中获取
    boolean cacheWasUsed = true;
    UserDetails user = this.userCache.getUserFromCache(username);
    if (user == null) {
    	cacheWasUsed = false;
        try {
        //3.检索User
        user = retrieveUser(username,(UsernamePasswordAuthenticationToken)authentication);
        }
    .....
    }
    try {
        //4. 认证前检查user状态
        preAuthenticationChecks.check(user);
        //5. 附加认证证检查
        additionalAuthenticationChecks(user,(UsernamePasswordAuthenticationToken) authentication);
    }
    .....
    //6. 认证后检查user状态
    postAuthenticationChecks.check(user);
    .....
    // 7. 创建认证成功的UsernamePasswordAuthenticationToken并将认证状态设置为true
    return createSuccessAuthentication(principalToReturn,authentication, user);
    }
```

retrieveUser方法

```java
protected final UserDetails retrieveUser(String username,UsernamePasswordAuthenticationToken authentication)
	throws AuthenticationException {
    prepareTimingAttackProtection();
    try {
        //调用自定义UserDetailsService的loadUserByUsername的方法
        UserDetails loadedUser = this.getUserDetailsService().loadUserByUsername(username);
        if (loadedUser == null) {
            throw new InternalAuthenticationServiceException(
            "UserDetailsService returned null, which is an interface contract violation");
        }
        return loadedUser;
    }
    ....
}
```

additionalAuthenticationChecks方法

```java
protected void additionalAuthenticationChecks(UserDetails userDetails,
	UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    .....
    // 1.获取前端密码
    String presentedPassword = authentication.getCredentials().toString();
    // 2.与数据库中的密码进行比对
    if (!passwordEncoder.matches(presentedPassword,userDetails.getPassword())) {
    	logger.debug("Authentication failed: password does not match stored value");
    	throw new BadCredentialsException(messages.getMessage(
    				"AbstractUserDetailsAuthenticationProvider.badCredentials","Bad credentials"));
    }
}
```

## 鉴权流程 Authorization

![20220405-110903-0137.png](./image/20220405-110903-0137.png)

### FilterSecurityInterceptor

鉴权过滤器为FilterSecurityInterceptor，FilterSecurityInterceptor的上一层过滤器是ExceptionTranslationFilter，ExceptionTranslationFilter会捕获FilterSecurityInterceptor抛出的AuthenticationException认证异常（未登录）或AccessDeniedException鉴权异常（无权限）

ExceptionTranslationFilter#doFilter

```java
public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
      throws IOException, ServletException {
   HttpServletRequest request = (HttpServletRequest) req;
   HttpServletResponse response = (HttpServletResponse) res;
   try {
      // 调用下一个过滤器即FilterSecurityInterceptor
      chain.doFilter(request, response);
      logger.debug("Chain processed normally");
   }
   catch (IOException ex) {
      throw ex;
   }
   catch (Exception ex) {
        .....
      	// 处理FilterSecurityInterceptor的异常
         handleSpringSecurityException(request, response, chain, ase);
      }
	  .....
   }
}
```

ExceptionTranslationFilter#doFilter

```java
private void handleSpringSecurityException(HttpServletRequest request,
      HttpServletResponse response, FilterChain chain, RuntimeException exception)
      throws IOException, ServletException {
   if (exception instanceof AuthenticationException) {
	  //认证异常  
      sendStartAuthentication(request, response, chain,
            (AuthenticationException) exception);
   }
   else if (exception instanceof AccessDeniedException) {
      Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
      if (authenticationTrustResolver.isAnonymous(authentication) || authenticationTrustResolver.isRememberMe(authentication)) {
        //认证异常  
         sendStartAuthentication(
               request,
               response,
               chain,
               new InsufficientAuthenticationException(
                  messages.getMessage(
                     "ExceptionTranslationFilter.insufficientAuthentication",
                     "Full authentication is required to access this resource")));
      }
      else {
		//鉴权异常
         accessDeniedHandler.handle(request, response,
               (AccessDeniedException) exception);
      }
   }
}
```

> authenticationEntryPoint处理认证异常  ，accessDeniedHandler处理鉴权异常

FilterSecurityInterceptor#doFilter

```java
public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) 
    throws IOException, ServletException {
    FilterInvocation fi = new FilterInvocation(request, response,chain);
    // 调用
    invoke(fi);
    }


public void invoke(FilterInvocation fi) throws IOException, ServletException{
    if ((fi.getRequest() != null)
    	.....
    } else {
        ...
        //前置调用
        InterceptorStatusToken token = super.beforeInvocation(fi);
        ....
        // 后置调用
        super.afterInvocation(token, null);
    }
}
```

AbstractSecurityInterceptor的beforeInvocation方法

```java
protected InterceptorStatusToken beforeInvocation(Object object) {
    // 1. 获取security的系统配置权限
    Collection<ConfigAttribute> attributes = this.obtainSecurityMetadataSource().getAttributes(object);
    .....
    // 2. 获取用户认证的信息
    Authentication authenticated = authenticateIfRequired();
    // Attempt authorization
    try {
        // 3.调用决策管理器
        this.accessDecisionManager.decide(authenticated, object, attributes);
    } catch (AccessDeniedException accessDeniedException) {
        publishEvent(new AuthorizationFailureEvent(object, attributes,authenticated,accessDeniedException));
        // 4.无权限则抛出异常让ExceptionTranslationFilter捕获
        throw accessDeniedException;
    }
    ....
}
```

AffirmativeBased的decide方法

```java
public void decide(Authentication authentication, Object object, 			       			         Collection<ConfigAttribute>configAttributes) throws AccessDeniedException {
    int deny = 0;
    for (AccessDecisionVoter voter : getDecisionVoters()) {
    	int result = voter.vote(authentication, object,configAttributes);
    	if (logger.isDebugEnabled()) {
        logger.debug("Voter: " + voter + ", returned: " + result);
    	}
    	switch (result) {
    	//一票通过，只要有一个投票器通过就允许访问
    	case AccessDecisionVoter.ACCESS_GRANTED:
            return;
    	case AccessDecisionVoter.ACCESS_DENIED:
    		deny++;
    		break;
    	default:
    		break;
    	}
    }
    if (deny > 0) {
        throw new AccessDeniedException(messages.getMessage(
        "AbstractAccessDecisionManager.accessDenied", "Access is denied"));
    }
    ...
}
```

## 过滤器链

 Spring Security默认加载15个过滤器

1. org.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFilter

    根据请求封装获取WebAsyncManager，从WebAsyncManager获取/注册的安全上下文可调用处理拦截器 

2. org.springframework.security.web.context.SecurityContextPersistenceFilter 

   主要是使用SecurityContextRepository在session中保存或更新一个SecurityContext，并将SecurityContext给以后的过滤器使用，来为后续filter 建立所需的上下文。SecurityContext中存储了当前用户的认证以及权限信息。

   > 设置SecurityContextRepository可以通过token获取SecurityContext，实现token登录

3. org.springframework.security.web.header.HeaderWriterFilter 

   向response的Header中添加相应的信息,可在http标签内部使用security:headers来控制 

4.  org.springframework.security.web.csrf.CsrfFilter 

   csrf又称跨域请求伪造，SpringSecurity会对所有post请求验证是否包含系统生成的csrf的 token信息，如果不包含，则报错。起到防止csrf攻击的效果。 

5. org.springframework.security.web.authentication.logout.LogoutFilter  

   匹配URL为/logout的请求，实现用户退出,清除认证信息。 

   > 设置LogoutHandler、LogoutSuccessHandler可以定义退出逻辑，执行退出后不会再经过其他过滤器链

6. org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter 

​		表单认证操作全靠这个过滤器，默认匹配URL为/login且必须为POST请求。 

7. org.springframework.security.web.authentication.ui.DefaultLoginPageGeneratingFilter 

   如果没有在配置文件中指定认证页面，则由该过滤器生成一个默认认证页面。 

8. org.springframework.security.web.authentication.ui.DefaultLogoutPageGeneratingFilter 

   由此过滤器可以生产一个默认的退出登录页面 

9. org.springframework.security.web.authentication.www.BasicAuthenticationFilter 

   此过滤器会自动解析HTTP请求中头部名字为Authentication，且以Basic开头的头信息。 

10. org.springframework.security.web.savedrequest.RequestCacheAwareFilter 

    通过HttpSessionRequestCache内部维护了一个RequestCache，用于缓存 HttpServletRequest 

11. org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter 

    针对ServletRequest进行了一次包装，使得request具有更加丰富的API 

12. org.springframework.security.web.authentication.AnonymousAuthenticationFilter 

    当SecurityContextHolder中认证信息为空,则会创建一个匿名用户存入到 SecurityContextHolder中。spring security为了兼容未登录的访问，也走了一套认证流程， 只不过是一个匿名的身份。 

13. org.springframework.security.web.session.SessionManagementFilter 

​		securityContextRepository限制同一用户开启多个会话的数量 

7. org.springframework.security.web.access.ExceptionTranslationFilter 

   异常转换过滤器位于整个springSecurityFilterChain的后方，用来转换整个链路中出现的异常 

8. org.springframework.security.web.access.intercept.FilterSecurityInterceptor 

   获取所配置资源访问的授权信息，根据SecurityContextHolder中存储的用户信息来决定其是否有权限。
   
   

SecurityContextPersistenceFilter#doFilter

```java
public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
      throws IOException, ServletException {
   HttpServletRequest request = (HttpServletRequest) req;
   HttpServletResponse response = (HttpServletResponse) res;
   if (request.getAttribute(FILTER_APPLIED) != null) {
      // ensure that filter is only applied once per request
      chain.doFilter(request, response);
      return;
   }
   request.setAttribute(FILTER_APPLIED, Boolean.TRUE);
   if (forceEagerSessionCreation) {
      HttpSession session = request.getSession();

      if (debug && session.isNew()) {
         logger.debug("Eagerly created session: " + session.getId());
      }
   }
   HttpRequestResponseHolder holder = new HttpRequestResponseHolder(request,
         response);
   //从SecurityContext存储库取出SecurityContext，默认从session中取
   SecurityContext contextBeforeChainExecution = repo.loadContext(holder);
   try {
      SecurityContextHolder.setContext(contextBeforeChainExecution);
      chain.doFilter(holder.getRequest(), holder.getResponse());
   }
   finally {
      SecurityContext contextAfterChainExecution = SecurityContextHolder
            .getContext();
      SecurityContextHolder.clearContext();
      //SecurityContext存储库中存入SecurityContext，默认存入session
      repo.saveContext(contextAfterChainExecution, holder.getRequest(),
            holder.getResponse());
      request.removeAttribute(FILTER_APPLIED);
   }
}
```
