# 类加载

## 类加载过程

![20210313-125828-0631](./image/20210313-125828-0631.png)

类加载器子系统负责加载Class文件，ClassLoader只负责Class文件的加载，至于它是否可以运行，则由ExecutionEngine决定。

加载的类信息存放在方法区，方法区中还会存放运行时常量池信息，可能还包括字符串字面量和数字常量（这部分常量信息是Class文件常量池部分的内存映射）。

![20210313-181700-0316.png](./image/20210313-181700-0316.png)

> 加载

- 通过一个类的全限定类名获取此类的二进制字节流
- 将这个字节流所代表的静态储存结构转化为方法区的运行时数据
- 在内存中生成一个代表此类的java.lang.Class对象，作为方法区这个类各种数据的访问入口

> 验证

- 保证被加载类的正确性，不会危害虚拟机自身安全

> 准备

- 为类变量分配内存并且设置该类变量的默认初始值，final修饰的static在编译的时候就会分配，准备阶段会显示初始化
- 这里不会为实例变量分配初始化，类变量分配在方法区中，而实例变量会随着对象一起分配到Java堆中

> 解析

- 将常量池内的符号引用转换为直接引用的过程

> 初始化

- 初始化阶段就是执行类构造器方法<clinit>的过程
-  此方法不需要定义，是javac编译器自动收集类中所有类变量的赋值动作和静态代码块中的语句合并而来
- 构造器方法中指令按源文件中出现的顺序执行
-  <clinit> 不同于类的构造器，可以不存在，<init>一定存在
- 若该类具有父类，JVM会保证子类的<clinit>执行前，父类的<clinit>已经执行完毕。静态属性和静态方法可以被继承，不能被重写
- JVM必须保证一个类的<clinit>方法在多线程下被同步加锁

## 类加载器

### 分类

>  JVM支持的两种类型的类加载器，分别是引导类加载器（BootstrapClassLoader）和自定义类加载器（User-Definde ClassLoader），所有派生于抽象类ClassLoader的类加载器都划分为自定义类加载器

### 引导类加载器

（BootstrapClassLoader）

- 这个类加载器使用c/c++语言实现，嵌套在JVM内部，没有父加载器
- 用来加载Java的核心库(JAVA_HOME/jre/lib/rt.jar、resources.jar、sun.boot.calss.path路径下的内容)，用于提供JVM自身需要的类
- 加载扩展类和应用程序类加载器，并指定为他们的父加载器
- 出于安全考虑，只加载包名为java、javax、sun等开头的类

### 拓展类加载器

（Extension ClassLoader）

- 父加载器为引导类加载器

- 派生于ClassLoader类，由sun. misc.Launcher$ExtClasserLoader实现
- 从java.ext.dirs系统属性所指定的目录中加载类库，或从JDK的安装目录的jre/lib/ext子目录下加载类库。如果用户创建的jar放在此目录下，也会由此拓展类加载器加载

 ### 应用程序类加载器

(AppClassLoader)

- 父加载器为拓展类加载器，
- ExtClasserLoader和AppClassLoader不是继承关系，这两个类都继承自URLClassLoader
- 派生于ClassLoader类，由sun.misc.Launcher$AppClassLoader实现
- 负责加载环境变量classpath或系统属性java.class.path指定路径下的类库
- 是程序中默认的类加载器，一般Java应用的类都是由它完成加载

## 双亲委派机制

> Java虚拟机对class文件采用的是按需加载的方式，也就是说当需要使用该类时才会对它的class文件加载到内存生成class对象。加载class文件时，Java虚拟机采用的是双亲委派模式，即把请求交由父类处理。

![20210314-082245-0788.png](./image/20210314-082245-0788.png)

- 如果一个类加载器收到类加载请求，它并不会自己先去加载，而是把这个请求委托给父类的加载器去执行
- 如果父类加载器还存在父类加载器，则进一步向上委托，请求最终到达顶层的引导类加载器
- 如果父类加载器可以完成加载任务，就返回成功，若无法完成，交由子类加载器尝试。

> 避免类的重复加载
>
> 保护程序安全，防止核心API被随意篡改

```java
package java.lang;

public class String {
    public static void main(String[] args) {
        System.out.println("hello");
    }
}
```

![20210314-092315-0146.png](./image/20210314-092315-0146.png)

### 沙箱安全机制 

自定义String类，加载时会率先使用引导类加载器加载，而引导类加载器在加载过程中会先加载jdk自带的文件（rt.jar包中的java\lang\String.calss），报错信息说没有main方法就是因为加载的是rt.jar包中的String类。

